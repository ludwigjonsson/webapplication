﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Logic.Services.Abstractions;

namespace WebApplication.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        public readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }


        
        // GET: api/User
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_userService.GetUserCredentials());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // GET: api/User/5
        [HttpGet("{userID}", Name = "Get")]
        public IActionResult Get(int userID)
        {
            try
            {
                return Ok(_userService.GetUsername(userID));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }



        // POST: api/User
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
