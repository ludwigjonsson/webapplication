﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Logic.Services;
using WebApplication.Logic.Services.Abstractions;

namespace WebApplication.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        public readonly ITodoService _todoService;
        public TodoController(ITodoService todoService)
        {
            _todoService = todoService ?? throw new ArgumentNullException(nameof(todoService));
        }


        public IActionResult Get()
        {
            try
            {
                return Ok(_todoService.GetUserTodos(1));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [HttpGet]
        [Route("{userID}")]
        public IActionResult Get(int userID)
        {
            try
            {
                return Ok(_todoService.GetUserTodos(userID));
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        [HttpPost]
        [Route("createtodoitem/{HeadID}/{ItemText}")]
        public void CreateTodoItem(int HeadID, string ItemText)
        {
            _todoService.CreateTodoItem(HeadID, ItemText, false);
        }

        [HttpPut]
        [Route("updatetodoitem/{LineID}/{IsCompleted}")]
        public void UpdateTodoItem(int LineID, bool IsCompleted)
        {
            _todoService.UpdateTodoItem(LineID, IsCompleted);
        }
    }
}