using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebApplication.Logic.Services;
using WebApplication.Logic.Services.Abstractions;
using WebApplication.Logic.Data;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using WebApplication.Logic;
using WebApplication.Logic.Data.Models;

namespace WebApplication.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllersWithViews();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITodoService, TodoService>();
            services.AddSingleton<IMapper>(ConfigureMapper());
            services.AddDbContext<ApplicationContext>(x => x.UseInMemoryDatabase(databaseName: "ApplicationDb"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.      
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });

            using var scope = app.ApplicationServices.CreateScope();
            AddTestData(scope.ServiceProvider.GetRequiredService<ApplicationContext>());
        }

        private IMapper ConfigureMapper()
        {
            return new MapperConfiguration(cfg => cfg.AddProfile<ServiceProfile>()).CreateMapper();
        }

        //Create Data for our InMemoryDB to display in the application
        private void AddTestData(ApplicationContext context)
        {
            var User = new Logic.Data.Models.User
            {
                UserID = 1,
                Username = "admin",
                Password = "password",
                UserTodo = null
            };
            context.Users.Add(User);

            var User1 = new Logic.Data.Models.User
            {
                UserID = 2,
                Username = "user1",
                Password = "password"
            };
            context.Users.Add(User1);

            var User2 = new Logic.Data.Models.User
            {
                UserID = 3,
                Username = "user2",
                Password = "password"
            };
            context.Users.Add(User2);

            var Head = new TodoHead
            {
                HeadID = 1,
                HeadText = "List One"
            };
            context.TodoHeads.Add(Head);

            var Line1 = new TodoLine
            {
                LineID = 1,
                HeadID = 1,
                LineText = "Line 1",
                IsCompleted = false
            };

            var Line2 = new TodoLine
            {
                LineID = 2,
                HeadID = 1,
                LineText = "Line 2",
                IsCompleted = true
            };
            context.TodoLines.AddRange(Line1, Line2);

            var UserTodo1 = new UserTodo
            {
                UserID = 1,
                TodoHeadID = 1
            };
            context.UserLists.Add(UserTodo1);

            var Head1 = new TodoHead
            {
                HeadID = 2,
                HeadText = "List Two"
            };
            context.TodoHeads.Add(Head1);

            var Line3 = new TodoLine
            {
                LineID = 3,
                HeadID = 2,
                LineText = "List 2, Line 3",
                IsCompleted = false
            };
            context.TodoLines.Add(Line3);

            var UserTodo2 = new UserTodo
            {
                UserID = 1,
                TodoHeadID = 2
            };
            context.UserLists.Add(UserTodo2);

            context.SaveChanges();
        }
    }
}
