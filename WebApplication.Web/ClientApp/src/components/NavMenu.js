﻿import React, { Component, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Container, NavbarBrand, NavItem, NavbarToggler, Collapse, NavLink } from 'reactstrap';

export default function NavMenu() {
    const [collapsed, setCollapsed] = useState(false);

    return (
        <header>
            <Navbar className="navbar-expand-sm nabvar-toggletable-sm ng-white border-bottom box-shadow mb-3" light>
                <Container>
                    <NavbarBrand tag={Link} to="/">Ludwigs WebApplication</NavbarBrand>
                    <NavbarToggler onClick={() => setCollapsed(!collapsed)} className="mr-2" />
                    <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={collapsed} navbar>
                        <ul className="navbar-nav flex-grow">
                            <NavItem>
                                <NavLink tag={Link} className="text-dark" to="/">Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} className="text-dark" to="/login">Login</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink tag={Link} className="text-dark" to="/displaytodo">Todo</NavLink>
                            </NavItem>
                        </ul>
                    </Collapse>
                </Container>
            </Navbar>
        </header>
        );
}