﻿import React, { useState, useEffect } from 'react';
import { Collapse, ListGroup, ListGroupItem, Form, FormGroup, Input, Label, Button } from 'reactstrap';
import TodoItem from './TodoItem';
import '../styles/TodoStyle.css'

export default function TodoHead(props) {
    const [isOpen, setIsOpen] = useState(true);
    const [text, setText] = useState("");
    const id = props.id;

    const toggle = () => setIsOpen(!isOpen);

    const handleSubmit = event => {
        fetch('https://localhost:44326/api/todo/createtodoitem/' + id + '/' + text, { method: 'POST' });
    };


    return (
        <div>
            <ListGroupItem onClick={toggle} className='todohead'>{props.headText}</ListGroupItem>
            <Collapse isOpen={isOpen}>
                <ListGroup>
            {props.todoItems.map(todoitem =>
                <TodoItem id={todoitem.lineID} itemText={todoitem.itemText} isCompleted={todoitem.isCompleted} />
                    )}
                    <Form onSubmit={handleSubmit}>
                        <FormGroup>
                            <Label for="todoItemText" style={{ marginTop: '1rem' }}>Add a new Todo:</Label>
                            <Input type="textarea" name="text" id="todoItemText" onChange={event => setText(event.target.value)} />
                        </FormGroup>
                        <Button>Submit</Button>
                    </Form>
                </ListGroup>
                </Collapse>
            <hr />
        </div>
        );
}