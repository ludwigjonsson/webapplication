﻿import React, { useState, useEffect } from 'react';
import { ListGroupItem } from 'reactstrap'
import '../styles/TodoStyle.css'
import { render } from 'react-dom';

export default function TodoItem(props) {
    const [isCompleted, setIsCompleted] = useState(props.isCompleted);
    var itemClass = isCompleted ? "complete" : "incomplete";
    const id = props.id;

    function completeItem() {
        setIsCompleted(!isCompleted);
        fetch("https://localhost:44326/api/todo/updatetodoitem/" + id + '/' + !isCompleted, { method: 'PUT' });
    }


    return (
        <ListGroupItem className={itemClass} onClick={completeItem}>
            {props.itemText}
        </ListGroupItem>
        );
}