﻿import React, { useState, useEffect, Fragment } from 'react';
import TodoItem from './TodoItem';
import TodoHead from './TodoHead';

export default function DisplayTodo() {
    const [todoHead, setTodoHead] = useState([]);
    const [loading, setLoading] = useState(true);

    //Fetch data from API
    async function populateData() {
        const response = await fetch('https://localhost:44326/api/todo/');
        const data = await response.json();

        setTodoHead(data);
        setLoading(false);
    }

    //Populate data when component is mounted.
    useEffect(() => {
        populateData();
    }, []);

    return (
        <div>
            <h1>Todo Lists</h1>
            <div>
                {todoHead.map(todohead =>
                    <TodoHead id={todohead.headID} headText={todohead.headText} todoItems={todohead.todoItems} />
                    )}
                <span>Has Error: {loading}</span>
            </div>
        </div>
        );
}