﻿import React, { useState } from "react";
import { Button, FormGroup, Form, Label, Input,   } from "reactstrap";

export default function Login(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    function handleSubmit(event) {
        event.preventDefault();
    }

    return (
        <div>
            <Form>
                <FormGroup>
                    <Label for="exampleEmail">Username</Label>
                    <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
                </FormGroup>
                <FormGroup>
                    <Label for="examplePassword">Password</Label>
                    <Input type="password" name="password" id="examplePassword" placeholder="password placeholder" />
                </FormGroup>
                <Button>Login</Button>
            </Form>
        </div>
        );
}