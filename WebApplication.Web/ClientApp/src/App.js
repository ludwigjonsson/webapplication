import React, { Component } from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Login from './components/Login';
import DisplayTodo from './components/DisplayTodo';


export default function App(){
    return (
        <Layout>
            <Route exact path='/' component={Home} />
            <Route path="/login" component={Login} />
            <Route path="/displaytodo" component={DisplayTodo} />
        </Layout>
        );
}
