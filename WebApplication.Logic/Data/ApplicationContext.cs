﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using WebApplication.Logic.Data.Models;

namespace WebApplication.Logic.Data
{
   public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<TodoHead> TodoHeads { get; set; }
        public DbSet<TodoLine> TodoLines { get; set; }
        public DbSet<UserTodo> UserLists { get; set; }

        public ApplicationContext()
        {

        }
        public ApplicationContext(DbContextOptions<ApplicationContext> options): base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<User>(u =>
            {
                u.ToTable("Users");
            });

            //Creating a table that is connecting Users to Todolist, with a relationship of many-to-many
            //Reason for this being if in the future I'd like to be able to share TodoLists between different Users

            modelBuilder.Entity<UserTodo>(ut =>
            {
                ut.HasKey(t => new { t.UserID, t.TodoHeadID });

                ut.HasOne(ut => ut.User)
                .WithMany(u => u.UserTodo)
                .HasForeignKey(ut => ut.UserID);

                ut.HasOne(ut => ut.TodoHead)
                .WithMany(th => th.UserTodo)
                .HasForeignKey(ut => ut.TodoHeadID);

            });


            modelBuilder.Entity<TodoHead>(t =>
            {
                t.ToTable("TodoHeads");
            });

            modelBuilder.Entity<TodoLine>(i =>
            {
                i.ToTable("TodoLines");
                i.HasOne(th => th.TodoHead)
                .WithMany(tl => tl.TodoItems)
                .HasForeignKey(x => x.HeadID);
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
