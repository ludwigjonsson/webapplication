﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Logic.Data.Models
{
   public class UserTodo
    {
        public int UserID { get; set; }
        public int TodoHeadID { get; set; }
        public User User { get; set; }
        public TodoHead TodoHead { get; set; }

    }
}
