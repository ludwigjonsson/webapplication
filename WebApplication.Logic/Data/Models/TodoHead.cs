﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Logic.Data.Models
{
    public class TodoHead
    {
        [Key]
        public int HeadID { get; set; }
        public string HeadText { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public virtual ICollection<UserTodo> UserTodo { get; set; }
        public virtual ICollection<TodoLine> TodoItems { get; set; }
    }
}

