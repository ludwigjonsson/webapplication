﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication.Logic.Data.Models
{
    public class TodoLine
    {
        [Key]
        public int LineID { get; set; }
        public string LineText { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public int HeadID { get; set; }
        public TodoHead TodoHead { get; set; }
    }
}
