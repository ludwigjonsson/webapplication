﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Logic.Data;
using WebApplication.Logic.Services.Abstractions;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace WebApplication.Logic.Services
{
   public class UserService : IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly ApplicationContext _context;

        public UserService(ILogger<UserService> logger, ApplicationContext context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _context = context ?? throw new ArgumentNullException(nameof(logger));
        }

        public string GetUsername(int userID)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserID.Equals(userID));

            return user?.Username ?? "User doesnt Exist";
        }

        public List<User> GetUserCredentials()
        {
            var credentials = _context.Users.Select(u => new User
            {
                Username = u.Username,
                Password = u.Password
            }).ToList();

            return credentials;
        }
    }
}
