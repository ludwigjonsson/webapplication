﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Logic.Services
{
    //Inconsistency with model, should be renamed to TodoLine so its clear what this class represents
    public class TodoItem
    {
        public int lineID { get; set; }
        public string ItemText { get; set; }
        public bool isCompleted { get; set; }
    }
}
