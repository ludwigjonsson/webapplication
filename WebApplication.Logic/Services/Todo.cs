﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Logic.Services
{

    //Inconsistensy with model, should be renamed to TodoHead so its clear what this class represents
    public class Todo
    {
        public int HeadID { get; set; }
        public string HeadText { get; set; }
        public List<TodoItem> TodoItems { get; set; }
    }
}
