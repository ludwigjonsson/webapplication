﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Logic.Data;
using WebApplication.Logic.Services.Abstractions;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace WebApplication.Logic.Services
{
    public class TodoService : ITodoService
    {
        private readonly ILogger<TodoService> _logger;
        private readonly ApplicationContext _context;
        private readonly IMapper _mapper;

        public TodoService(ILogger<TodoService> logger, ApplicationContext context, IMapper mapper)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _context = context ?? throw new ArgumentNullException(nameof(logger));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(logger));
        }

        public List<Todo> GetUserTodos(int UserID)
        {
            return _mapper.Map<List<Todo>>(_context.TodoHeads.Include(x => x.TodoItems).Where(x => x.UserTodo.Any(x => x.UserID == UserID)));
        }

        //Create the table for connecting a user to a TodoList
        public void CreateUserTodo(int UserID, string HeadText)
        {
            var newTodoHeadID = _context.TodoHeads.Max(h => h.HeadID);

            _context.UserLists.Add(new Data.Models.UserTodo
            {
                UserID = UserID,
                TodoHeadID = newTodoHeadID + 1,
            });

            _context.TodoHeads.Add(new Data.Models.TodoHead
            {
                HeadID = newTodoHeadID + 1,
                HeadText = HeadText,
            });

            _context.SaveChanges();
        }

        public void CreateTodoItem(int HeadID, string ItemText, bool IsCompleted)
        {
            var newTodoItemID = _context.TodoLines.Max(l => l.LineID);

            _context.TodoLines.Add(new Data.Models.TodoLine
            {
                LineID = newTodoItemID + 1,
                HeadID = HeadID,
                LineText = ItemText,
                IsCompleted = IsCompleted
            });

            _context.SaveChanges();
        }

        public void UpdateTodoItem(int LineID, bool IsCompleted)
        {
            var todoItemToBeChanged = _context.TodoLines.FirstOrDefault(t => t.LineID == LineID);

            todoItemToBeChanged.IsCompleted = IsCompleted;
            _context.TodoLines.Update(todoItemToBeChanged);

            _context.SaveChanges();
        }
    }
}
