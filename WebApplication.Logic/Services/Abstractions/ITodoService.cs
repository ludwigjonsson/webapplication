﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication.Logic.Data.Models;

namespace WebApplication.Logic.Services.Abstractions
{
    public interface ITodoService
    {
        List<Todo> GetUserTodos(int UserID);

        public void CreateUserTodo(int UserID, string HeadText);
        public void CreateTodoItem(int HeadID, string ItemText, bool IsCompleted);

        public void UpdateTodoItem(int LineID, bool IsCompleted);

    }
}
