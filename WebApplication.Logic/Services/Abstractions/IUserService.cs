﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication.Logic.Services.Abstractions
{
    public interface IUserService
    {
        string GetUsername(int userID);
        List<User> GetUserCredentials();
    }
}
