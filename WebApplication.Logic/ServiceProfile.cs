﻿using AutoMapper;
using WebApplication.Logic.Data.Models;
using WebApplication.Logic.Services;

namespace WebApplication.Logic
{
    public class ServiceProfile : Profile
    {
        //Create mapping for different members here, as shown by a colleague for less text in the service class.
        public ServiceProfile()
        {
            CreateMap<TodoHead, Todo>()
                .ForMember(x => x.TodoItems, m => m.MapFrom(x => x.TodoItems))
                .ForMember(x => x.HeadID, m => m.MapFrom(x => x.HeadID));
            CreateMap<TodoLine, TodoItem>()
                .ForMember(x => x.ItemText, m => m.MapFrom(x => x.LineText))
                .ForMember(x => x.isCompleted, m => m.MapFrom(x => x.IsCompleted))
                .ForMember(x => x.lineID, m => m.MapFrom(x => x.LineID));
            //CreateMap<TodoLine, Todo>().ForMember(x=>x.TodoItems, m=> m.MapFrom(x=>x.)
        }
    }
}
